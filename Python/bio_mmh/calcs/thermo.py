#!/usr/bin/env python
# coding: utf-8

# In[2]:


import numpy as np
import pandas as pd
import os
import math
import re
import sys
from scipy import stats
import seaborn as sns
import matplotlib.pyplot as plt
import itertools
from scipy.optimize import minimize
from numba import jit


# In[3]:


from github import Github
import requests


# In[44]:


g = Github('64a2f2f25819e248f4c1eb9d69902a03294e6dd1')


# In[86]:


with open(os.path.join(sys.path[0], 'version.txt'), 'r', encoding='UTF-8') as version_info:
    
    this_version = version_info.readlines()

repo = g.get_user().get_repo('Biostatistics-MMH')
contents = repo.get_contents("Python/Thz.py")

if contents.sha != this_version[0]:
    
    update = input('You are using an outdated script. Type "update" to update or "skip" to skip: \n')
    
    if update.lower() == 'update':
    
        date = contents.last_modified
        date = re.sub('[ :]', '_', date[5:-4])
        url = contents.download_url
        new_code = requests.get(url)

        with open(os.path.join(sys.path[0], 'THz_'+date+'.py'), 'w+') as new:
            new.write(new_code.text)

        with open(os.path.join(sys.path[0], 'version.txt'), 'w+', encoding='UTF-8') as version_info:
            version_info.write(contents.sha)
        
        print('Update completed. Look for a new version in the current script folder')
        quit()


# In[18]:


with open(os.path.join(sys.path[0], 'Options.txt'), encoding='UTF-8') as options:
    opt = [re.sub('\n', '', line) for line in options.readlines()]


# In[5]:


path_n = opt[0].split(' = ')[1]
path_tr = opt[1].split(' = ')[1]
path_output = opt[2].split(' = ')[1]
path_statistics = opt[3].split(' = ')[1]
path_optim = opt[4].split(' = ')[1]
length = float(opt[5].split(' = ')[1])
conc = float(opt[6].split(' = ')[1])
lower = float(opt[7].split(' = ')[1])
upper = float(opt[8].split(' = ')[1])

t1 = 8.28
eps = 2.5

s0 = conc


# In[23]:


def Parameters():
    samples = list(set([file.split(' ')[0] for file in os.listdir(path_n)]))

    files = sorted(os.listdir(path_n))   #Got alphabetically sorted files in n-folder

    for sample in samples:
        exec("result_" + re.sub('\.', '_', sample) + "e1" + " = dict()")   #Initialize a dictionary for each sample

    if len(samples) > 3:
        for sample in samples:
            counter = 0
            with pd.ExcelWriter(path_output + 'Result_' + sample +'.xlsx') as writer:
               
                result_e1 = dict()
                result_e2 = dict()
                result_e1_K = dict()
                result_e2_K = dict()

                for file in files:
                    if file.startswith(sample):
                        n = pd.read_csv(path_n+file, skiprows=1)   #reading the files
                        tr = pd.read_csv(path_tr+file, skiprows=1)

                        n = n[n['Wavenumber (cm-1)'].between(lower,upper)]   #cutting everything beyond 10 and above 100 cm-1
                        tr = tr[tr['Wavenumber (cm-1)'].between(lower,upper)]

                        e1 = n['Refractive Index']**2-(np.log(tr['Transmittance'])/(4*math.pi*n['Wavenumber (cm-1)']*length))**2
                        e2 = -2*n['Refractive Index']*np.log(tr['Transmittance'])/(4*math.pi*n['Wavenumber (cm-1)']*length)
                        e2_K = (((3*0.00071*conc-1)*2.89*e1+2*(e2**2-e1**2))*(2-3*0.00071*conc)*e2-(2.89+(2-3*0.00071*conc)*e1)*((3*0.00071*conc-1)*2.89*e2-4*e1*e2))/(((2-3*0.00071*conc)*e2)**2+(2.89+(2-3*0.00071*conc)*e1)**2)
                        e1_K = e2_K*((2-3*0.00071*conc)*e2)/(2.89+(2-3*0.00071*conc)*e1)-((3*0.00071*conc-1)*2.89*e1+2*(e2**2-e1**2))/(2.89+(2-3*0.00071*conc)*e1)

                        counter += 1
                        result_e1['e1_'+str(counter)] = e1
                        result_e2['e2_'+str(counter)] = e2
                        result_e1_K['e1_K_'+str(counter)] = e1_K
                        result_e2_K['e2_K_'+str(counter)] = e2_K

                result_e1 = pd.concat([n['Wavenumber (cm-1)'], pd.DataFrame(result_e1)], axis=1) 
                result_e2 = pd.concat([n['Wavenumber (cm-1)'], pd.DataFrame(result_e2)], axis=1) 
                result_e1_K = pd.concat([n['Wavenumber (cm-1)'], pd.DataFrame(result_e1_K)], axis=1)
                result_e2_K = pd.concat([n['Wavenumber (cm-1)'], pd.DataFrame(result_e2_K)], axis=1)

                result_e1.to_excel(writer, sheet_name = sample + '_e1', index=False)
                result_e2.to_excel(writer, sheet_name = sample +'_e2', index=False)
                result_e1_K.to_excel(writer, sheet_name = sample +'_e1_K', index=False)
                result_e2_K.to_excel(writer, sheet_name = sample + '_e2_K', index=False)

    else:    
        with pd.ExcelWriter(path_output+'Result_combined.xlsx') as writer:
            for sample in samples:
                counter = 0
                
                result_e1 = dict()
                result_e2 = dict()
                result_e1_K = dict()
                result_e2_K = dict()

                for file in files:
                    if file.startswith(sample):
                        n = pd.read_csv(path_n+file, skiprows=1)   #reading the files
                        tr = pd.read_csv(path_tr+file, skiprows=1)

                        n = n[n['Wavenumber (cm-1)'].between(lower,upper)]   #cutting everything beyond 10 and above 100 cm-1
                        tr = tr[tr['Wavenumber (cm-1)'].between(lower,upper)]

                        e1 = n['Refractive Index']**2-(np.log(tr['Transmittance'])/(4*math.pi*n['Wavenumber (cm-1)']*length))**2
                        e2 = -2*n['Refractive Index']*np.log(tr['Transmittance'])/(4*math.pi*n['Wavenumber (cm-1)']*length)
                        e2_K = (((3*0.00071*conc-1)*2.89*e1+2*(e2**2-e1**2))*(2-3*0.00071*conc)*e2-(2.89+(2-3*0.00071*conc)*e1)*((3*0.00071*conc-1)*2.89*e2-4*e1*e2))/(((2-3*0.00071*conc)*e2)**2+(2.89+(2-3*0.00071*conc)*e1)**2)
                        e1_K = e2_K*((2-3*0.00071*conc)*e2)/(2.89+(2-3*0.00071*conc)*e1)-((3*0.00071*conc-1)*2.89*e1+2*(e2**2-e1**2))/(2.89+(2-3*0.00071*conc)*e1)

                        counter += 1
                        result_e1['e1_'+str(counter)] = e1
                        result_e2['e2_'+str(counter)] = e2
                        result_e1_K['e1_K_'+str(counter)] = e1_K
                        result_e2_K['e2_K_'+str(counter)] = e2_K

                result_e1 = pd.concat([n['Wavenumber (cm-1)'], pd.DataFrame(result_e1)], axis=1) 
                result_e2 = pd.concat([n['Wavenumber (cm-1)'], pd.DataFrame(result_e2)], axis=1) 
                result_e1_K = pd.concat([n['Wavenumber (cm-1)'], pd.DataFrame(result_e1_K)], axis=1)
                result_e2_K = pd.concat([n['Wavenumber (cm-1)'], pd.DataFrame(result_e2_K)], axis=1)

                result_e1.to_excel(writer, sheet_name = sample + '_e1', index=False)
                result_e2.to_excel(writer, sheet_name = sample +'_e2', index=False)
                result_e1_K.to_excel(writer, sheet_name = sample +'_e1_K', index=False)
                result_e2_K.to_excel(writer, sheet_name = sample + '_e2_K', index=False)
        
    print('Parameters calculation done!')


# ### Statistical comparison

# In[8]:


def Welch (a, b):
    return(stats.ttest_ind(a, b, equal_var = False)[1])


# In[35]:


def Statistic():
    
    
    def progressBar(current, total, barLength = 20):
        percent = float(current) * 100 / total
        arrow   = '-' * int(percent/100 * barLength - 1) + '>'
        spaces  = ' ' * (barLength - len(arrow))

        print('Progress: [%s%s] %d %%' % (arrow, spaces, percent), end='\r')

        
    samples = []

    for file in os.listdir(path_output):
        if file.endswith('xlsx'):
            samples.append(pd.ExcelFile(path_output+file, engine='openpyxl').sheet_names)

    flat_list = list(set([re.sub('_e1_K|_e2_K|_e1|_e2', '', item) for sublist in samples for item in sublist]))

    print('We have found the following samples:\n')

    for n,i in enumerate(flat_list):
        print(n,i)

    print('\nWhich ones do you want to compare?')

    select = input('\nType the numbers, separated with comma (they will be compared pairwise). Type "exit" to exit the script: ')
    
    if select.lower() == 'exit':
        raise ValueError('Interrupted by user')
    
    select = re.sub(' ', '', select)
    select = select.split(',')

    select = [[i for n,i in enumerate(flat_list) if n == int(sel)] for sel in select]
    select = [item for sublist in select for item in sublist]

    pairs = list(itertools.combinations(select, 2))
    
    print('\n\nChoose pairs that you want to drop')
    
    for n,i in enumerate(pairs):
        print(n,i)
    
    print('\n')
    blacklist = input('Type the numbers, separated with comma. If none, type "n" and ENTER: ')
    blacklist = re.sub(' ', '', blacklist)
    
    if blacklist.lower() != 'n':
        
        blacklist = blacklist.split(',')
        pairs = [[i for n,i in enumerate(pairs) if n == int(drop)] for drop in blacklist]
        pairs = [item for sublist in pairs for item in sublist]
    
    progress = 4*len(pairs)
    iter_count = 0
    
    print('\nThe progress bar will appear as soon as the first comparison will be made. Whait a second or two...\n')
    
    for pair in pairs:
        sample_1_sheets = dict()
        sample_2_sheets = dict()
        
        for result in os.listdir(path_output):
            exl = pd.ExcelFile(path_output+result, engine='openpyxl')

            for i in exl.sheet_names:
                if pair[0] in i:
                    for param in ['e1', 'e2', 'e1_K', 'e2_K']:
                        if i.endswith(param):
                            sample_1_sheets[param] = pd.read_excel(path_output + result, sheet_name=i, engine='openpyxl')
                if pair[1] in i:
                    for param in ['e1', 'e2', 'e1_K', 'e2_K']:
                        if i.endswith(param):
                            sample_2_sheets[param] = pd.read_excel(path_output + result, sheet_name=i, engine='openpyxl')
        
        for param in sample_1_sheets.keys():   
            iter_count += 1
 
            s1 = pd.melt(sample_1_sheets[param], id_vars = 'Wavenumber (cm-1)')
            s1['Sample'] = pair[0]
            s1.columns = ['Wavenumber', 'Parameter', 'value', 'Sample']

            s2 = pd.melt(sample_2_sheets[param], id_vars = 'Wavenumber (cm-1)')
            s2['Sample'] = pair[1]
            s2.columns = ['Wavenumber', 'Parameter', 'value', 'Sample']

            pvals = pd.DataFrame({'Wavenumber': s1['Wavenumber'].unique(), 
                                  'p-value': [Welch(group['value'][group['Sample'] == pair[0]], group['value'][group['Sample'] == pair[1]]) for name, group in pd.concat([s1, s2], axis=0).groupby('Wavenumber')]})

            sns.lineplot(x="Wavenumber", y="value", hue='Sample', data=pd.concat([s1, s2], axis=0))
            ax2 = plt.twinx()
            sns.lineplot(x="Wavenumber", y="p-value", data=pvals, color="b", ax=ax2, legend=False)

            if pvals.max()['p-value'] > 0.05:
                ax2.axhline(0.05, ls='--')

            plt.savefig(path_statistics + pair[0] + '_'+ pair[1] + '_'+param+ '.png', dpi=300)
            plt.clf()

            pvals.to_excel(path_statistics + pair[0]+'_'+pair[1]+ '_'+param+'.xlsx', index=False)
            
            progressBar(iter_count, progress)


# In[ ]:


@jit(nopython=True)
def to_optim(p0, x, e1_K_i, e2_K_i):
    de1 = p0[0]
    de2 = p0[1]
    t2 = p0[2]
    A = p0[3]
    w2 = p0[4]
    g = p0[5]
    e1_mod = eps + de1/(1 + (0.1885*t1*x)*(0.1885*t1*x)) + de2/(1 + (0.1885*t2*x)*(0.1885*t2*x)) + (A*(w2**2-x**2))/((w2**2-x**2)*(w2**2-x**2)+(g*x)*(g*x))
    e2_mod = (0.1885*de1*t1*x)/(1 + (0.1885*t1*x)*(0.1885*t1*x)) + (0.1885*de2*t2*x)/(1.0 + (0.1885*t2*x)*(0.1885*t2*x)) + (A*g*x)/((w2*w2-x**2)*(w2**2-x**2)+ (g*x)*(g*x)) + 0.6*s0/x
    mse = np.sqrt(np.sum(((e2_mod-e2_K_i)/e2_mod)**2 + ((e1_mod-e1_K_i)/e1_mod)**2)) / len(x)
    return(mse)


# In[1]:


def SixPack():
    if not os.path.exists('/'.join(path_output.split('/')[0:-2])+'/Fits'):
        os.mkdir('/'.join(path_output.split('/')[0:-2])+'/Fits')
    
    for file in os.listdir(path_output):
        if file.startswith('Result'):
            excel = pd.ExcelFile(path_output+file, engine='openpyxl')
            unique = list(set([re.sub('_e[\d+]_K|_e[\d+]', '',i) for i in excel.sheet_names]))

            for sample in unique:
                mins = pd.DataFrame()
                for sheet in excel.sheet_names:
                    if sheet.startswith(sample):
                        if sheet.endswith('e1_K'):
                            e1_K = pd.read_excel(path_output+file, sheet_name=sheet, engine="openpyxl")
                        if sheet.endswith('e2_K'):
                            e2_K = pd.read_excel(path_output+file, sheet_name=sheet, engine="openpyxl")

                for repeat in range(1, len(e1_K.columns)):

                    e2_K_i = e2_K['e2_K_'+str(repeat)].to_numpy()
                    e1_K_i = e1_K['e1_K_'+str(repeat)].to_numpy()

                    x = e1_K['Wavenumber (cm-1)'].to_numpy()

                  
                    #Start optimization process
                    p0 = [48, 2, 0.2, 35000, 150, 280]
                    incr = 0
                    result_fun = np.full(shape=(100+incr,1), fill_value=999.9)
                    result_params = np.zeros(shape=(100+incr,6))
                    confirmations = 0
                    i = 0
                    previous_min = 100

                    try:
                        while confirmations < 6: 

                            res = minimize(to_optim, p0*np.random.rand(len(p0)), args = (x, e1_K_i, e2_K_i), method='L-BFGS-B', tol=1e-8, bounds=[(0,100), (0,10), (0,10),(0,200000),(0,300), (0,500)])
                            result_fun[i] = res.fun
                            result_params[i] = res.x
                            previous_min = result_fun.min()

                            if res.fun < previous_min:
                                previous_min = res.fun
                            elif res.fun == previous_min:
                                confirmations += 1
                            i += 1

                    except Exception as e:
                        #print(e)
                        incr += 100

                    min_params = result_params[np.argmin(result_fun)]
                    min_loss = result_fun[np.argmin(result_fun)]

                    mins = pd.concat([mins, pd.DataFrame({'sample': sample, 
                                                 'repeat': repeat,
                                                 'de1': min_params[0],
                                                 'de2': min_params[1],
                                                 't2': min_params[2],
                                                 'A': min_params[3],
                                                 'w2': min_params[4],
                                                 'g': min_params[5],
                                                 'loss': min_loss})], ignore_index=True)

                    plt.plot(x, eps + min_params[0]/(1 + (0.1885*t1*x)*(0.1885*t1*x)) + min_params[1]/(1 + (0.1885*min_params[2]*x)**2) + (min_params[3]*(min_params[4]**2-x**2))/((min_params[4]**2-x**2)*(min_params[4]**2-x**2)+(min_params[5]*x)**2))
                    plt.plot(x, e1_K_i) 
                    plt.savefig(path_optim + sample + '_' + str(repeat) + '_e1.png', dpi=300)
                    plt.clf()

                    plt.plot(x, (0.1885*min_params[0]*t1*x)/(1 + (0.1885*t1*x)*(0.1885*t1*x)) + (0.1885*min_params[1]*min_params[2]*x)/(1.0 + (0.1885*min_params[2]*x)**2) + (min_params[3]*min_params[5]*x)/((min_params[4]**2-x**2)*(min_params[4]**2-x**2)+ (min_params[5]*x)**2) + 0.6*s0/x)
                    plt.plot(x, e2_K_i) 
                    plt.savefig(path_optim + sample + '_' + str(repeat) + '_e2.png', dpi=300)
                    plt.clf()

                mins.to_excel(path_optim + sample+'.xlsx', engine='openpyxl', index=False)


# In[27]:


def Choise():
    choise = input('If you want to calculate the Ew spectra - press "E" and ENTER\nIf you want to compare spectra statisticaly - press "S" and ENTER\nIf you want to calculate optimizations and six main parameters - press "P" and ENTER\nIf you want to exit the script - type "exit"\n')
    return(choise)


# In[37]:


choise = 'ololo'

while(choise.lower() != 'n' or choise.lower() != 's'):
    choise = Choise()
    
    if choise.lower() == 's':
        Statistic()
    elif choise.lower() == 'e':
        Parameters()
    elif choise.lower() == 'p':
        SixPack()
    elif choise.lower() == 'exit':
        break
    else:
        print('Wrong symbol, type "E" or "S"\n')

