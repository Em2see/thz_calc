import os

base_path = os.environ.get("BASE_PATH")


class Config(object):
    DEBUG = False
    TESTING = False
    FLASK_ENV = "development"
    FOLDERS_PER_PAGE = 10
    FLASK_DEBUG = 1
    SECRET_KEY = "secret!"
    DEFAULT_FOLDER = os.path.join(base_path, 'files')
    SQLALCHEMY_DATABASE_URI = os.environ.get("FILES_DB_PATH")
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ProductionConfig(Config):
    pass


class DevelopmentConfig(Config):
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
