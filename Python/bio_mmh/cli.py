import click
import os
from flask import Flask
from flask_migrate import Migrate
from flask.cli import FlaskGroup
from flask.cli import AppGroup, pass_script_info
from .server import create_app, db

from .server.model import File, Folder


migrate = Migrate()


def manage_create_app():
    app = create_app()
    migrate.init_app(app, db)
    # other setup
    return app


@click.group(cls=FlaskGroup, create_app=manage_create_app)
@pass_script_info
def cli(info, **kwargs):
    """
       Management script for Color Report application.
    """
    pass

# @click.group(cls=FlaskGroup, create_app=manage_create_app)
# @click.option('--main-db', help='Path to the main db including username and password')
# @click.option('--imgs-db', help='Path to the images db including username and password')
# @pass_script_info
# def cli(info, **kwargs):
#     """
#        Management script for Color Report application.
#     """
#     # print(os.environ)
#     if kwargs['main_db'] is not None:
#         os.environ['MAIN_DB_URL'] = kwargs['main_db']
#     if kwargs['imgs_db'] is not None:
#         os.environ['IMGS_DB_URL'] = kwargs['imgs_db']
#     # print(info)
#     # print(kwargs)
#     print(os.environ.get('MAIN_DB_URL'))
#
#
# @cli.command()
# def add_admin_user():
#     """
#         Adding admin user to the database
#     """
#     session = db.session
#     user_name = click.prompt('Please enter username:')
#     try:
#         admin_role = session.query(User).filter(User.name == 'Admin').first()
#         admin_user = session.query(User).filter(
#             and_(User.first_name == user_name, User.role_id == admin_role.id)).first()
#     except Exception as ex:
#         print(ex)
#         admin_user = None
#     if admin_user is not None:
#         return None
#
#     user_password = click.prompt('Password:', hide_input=True)
#     tmp = User(
#         first_name=user_name,
#         password=user_password
#     )
#     session.add(tmp)
#     session.commit()
#
#
# @cli.command()
# def migrate_data():
#     """
#         Migrate data from old db
#         Color, User, Week, Right, Group, Note
#     """
#     migrator = MigrateDataFromOldDb()
#     migrator.connect(db_url_path=os.environ['OLD_DB_URL'])
#     res = migrator.get_db_data()
#     migrator.upload_data(res, db)
#     migrator.upload_references(res, db)
#
#
# @cli.command()
# def clear_data():
#     """
#         Clear data from new db
#         Color, User, Week, Right, Group, Note
#     """
#     session = db.session
#     for table in [Color, User, Week, Right, Group, Note, Notification, Role]:
#         table.query.delete()
#
#     for table in [membership, user_rights, group_leaders]:
#         table.delete()
#
#     session.commit()
