# -*- coding: utf-8 -*-

__author__ = 'Pavel Bobyrev, Dmitry Petrov'
__email__ = 'paul@mail.ru, e2m3x4@mail.ru'
__version__ = '0.5.1'

from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())

from .cli import cli
from .cli import manage_create_app as create_app

__all__ = ['cli', 'create_app']
