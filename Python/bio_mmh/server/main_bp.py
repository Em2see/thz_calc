from flask import Blueprint, url_for, make_response, send_from_directory
from flask import jsonify, send_file, request, g, render_template, current_app, redirect
from flask import flash
from werkzeug.utils import secure_filename
import os
import uuid
import pkg_resources
from tempfile import TemporaryDirectory

# STATIC_FILES_PATH = pkg_resources.resource_filename(__name__, 'static_files')

main_bp = Blueprint('main_bp', __name__, template_folder='./templates', static_folder='./static_files', static_url_path='/static')


@main_bp.record
def record_params(setup_state):
    app = setup_state.app
    main_bp.config = {key: value for key, value in app.config.items()}
    main_bp.logger = app.logger


@main_bp.route('/')
def index():
    return render_template('index.html')


@main_bp.route('/', methods=['POST'])
def index_post():
    return render_template('index.html')


def save_files(tmp_folder, refractive_files, transmittance_files):
    # tmp_folder = TemporaryDirectory()

    refractive_files_folder = os.path.join(tmp_folder, 'refractive')
    transmittance_files_folder = os.path.join(tmp_folder, 'transmittance')
    results_files_folder = os.path.join(tmp_folder, 'results')

    for file in refractive_files:
        # print(dir(file.name))
        file.save(refractive_files_folder)

    for file in transmittance_files:
        # print(file.name)
        file.save(transmittance_files_folder)

    return refractive_files_folder, transmittance_files_folder, results_files_folder
    # tmp_folder.cleanup()


@main_bp.route('/send_data', methods=['POST'])
def send_data():
    params = {k:v for k, v in request.form.items()}
    print(params)

    refractive_files = request.files.getlist('refractive_path_hidden')
    transmittance_files = request.files.getlist('transmittance_path_hidden')
    
    with TemporaryDirectory() as tmp_folder: 
        folders = save_files(tmp_folder, refractive_files, transmittance_files)
        refractive_files_folder, transmittance_files_folder, results_files_folder = folders
        # we need to

    return jsonify('yes')

