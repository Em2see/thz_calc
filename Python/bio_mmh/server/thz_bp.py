from flask import Blueprint, url_for, make_response, send_from_directory
from flask import jsonify, send_file, request, g, render_template, current_app, redirect
from flask import flash
from werkzeug.utils import secure_filename
import os
import uuid
import pkg_resources
from tempfile import TemporaryDirectory
from collections import defaultdict
from ..calcs import thz

STATIC_FILES_PATH = pkg_resources.resource_filename(__name__, 'static_files')

thz_bp = Blueprint('thz_bp', __name__, template_folder='./templates', static_folder='./static_files', static_url_path='/static')


@thz_bp.record
def record_params(setup_state):
    app = setup_state.app
    thz_bp.config = {key: value for key, value in app.config.items()}
    thz_bp.logger = app.logger


@thz_bp.route('/')
def index():
    return render_template('thz.html')

