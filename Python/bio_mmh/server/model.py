from .app import db
from datetime import datetime, timedelta, date
import json
from sqlalchemy import inspect
import os

DEFAULT_FOLDER_PATH_LEN = 256
DEFAULT_FILE_PATH_LEN = 64
DEFAULT_NAME_LEN = 64
FOLDER_LIFETIME = timedelta(weeks=1)


class File(db.Model):
    __table_name__ = 'files'
    
    id = db.Column(db.Integer, primary_key=True)
    path = db.Column(db.String(DEFAULT_NAME_LEN), nullable=False)
    name = db.Column(db.String(DEFAULT_NAME_LEN), nullable=False)
    size = db.Column(db.Integer, nullable=False)
    date_created = db.Column(db.DateTime, nullable=False, default=(lambda x: datetime.utcnow()))
    folder_id = db.Column(db.Integer, db.ForeignKey('folder.id'))
    folder = db.relationship("Folder", back_populates="files")

    def as_dict(self):
        result = {}
        keep_set = ['id', 'name', 'date_created', 'size', 'folder_id']

        for c in inspect(self).mapper.column_attrs:
            if (keep_set is None) or (c.key in keep_set):
                result[c.key] = getattr(self, c.key)
        
        result['path'] = os.path.join(self.folder.path, self.path)
        return result


class Folder(db.Model):
    __table_name__ = 'folders'

    id = db.Column(db.Integer, primary_key=True)
    path = db.Column(db.String(DEFAULT_FOLDER_PATH_LEN), nullable=False)
    files = db.relationship('File', back_populates="folder")
    date_created = db.Column(db.DateTime, nullable=False, default=(lambda x: datetime.utcnow()))
    date_timeout = db.Column(db.DateTime, nullable=False, default=(lambda x: datetime.utcnow() + FOLDER_LIFETIME))


__all__ = ['File', 'Folder']