from flask import Blueprint, url_for, make_response, send_from_directory
from flask import jsonify, send_file, request, g, render_template, current_app, redirect
from flask import flash
from werkzeug.utils import secure_filename
from sqlalchemy import asc, desc, or_
import os
import uuid
import pkg_resources
import uuid
from .model import Folder, File
from .app import db

STATIC_FILES_PATH = pkg_resources.resource_filename(__name__, 'static_files')

files_bp = Blueprint('files_bp', __name__, template_folder='./templates', static_folder='./static_files', static_url_path='/static')


@files_bp.record
def record_params(setup_state):
    app = setup_state.app
    files_bp.config = {key:value for key,value in app.config.items()}
    files_bp.logger = app.logger


@files_bp.route('/')
def index():
    page = request.args.get('page', 0, type=int)
    folder_id = request.args.get('folder_id', None, type=int)
    sorting = request.args.get('sorting', 'date_created', type=str)
    sorting_order = request.args.get('sorting_order', 'desc', type=str)

    if sorting_order == 'desc':
        sorting_func = desc
    else:
        sorting_func = asc
    rows = Folder.query.order_by(sorting_func(Folder.__dict__[sorting])).paginate(
        page, files_bp.config['FOLDERS_PER_PAGE'], False)
    
    files = None
    if folder_id is not None:
        files = File.query.filter(File.folder_id == folder_id).all()
        files = [f.as_dict() for f in files]

    return render_template('files.html', 
        folders=rows.items, 
        pages=rows.pages, 
        page=page, 
        sorting=sorting, 
        sorting_order=sorting_order,
        files=files
    )


@files_bp.route('/get_files', defaults={'folder_id': None})
@files_bp.route('/get_files/<folder_id>', methods=['POST'])
def get_files(folder_id):
    files = File.query.filter(File.folder_id == folder_id).all()
    return jsonify('yes')


def upload_files_local(files_list):
    default_path = current_app.config['DEFAULT_FOLDER']
    while True:
        folder_path = os.path.join(default_path, uuid.uuid4().hex)
        if not os.path.exists(folder_path):
            break
    
    folder = Folder(path=folder_path)
    os.mkdir(folder_path)
    db.session.add(folder)
    for file_storage in files_list:
        file_path = os.path.join(folder.path, file_storage.filename)
        file_storage.save(file_path)
        file_stat = os.stat(file_path)
                
        nfile = File(folder=folder, name=file_storage.filename, path=os.path.join('.', file_storage.filename), size=file_stat.st_size)
        db.session.add(nfile)

    db.session.commit()

    files = File.query.filter(File.folder_id == folder.id).all()

    files = {file.id:file.name for file in files}

    return {
        "folder_id": folder.id,
        "files": files
    }


@files_bp.route('/upload_files',  methods=['POST', 'GET'])
def upload_files():
    for k in request.files.keys():
        files_list = request.files.getlist(k)
        res = upload_files_local(files_list)
    print(res)
    return jsonify(res)


@files_bp.route('/delete', defaults={'folder_id': None})
@files_bp.route('/delete/<folder_id>', methods=['POST', 'GET'])
def delete(folder_id):
    out_files = []

    folder = Folder.query.filter(Folder.id == folder_id).first()

    for file in folder.files:
        fdict = file.as_dict()
        if os.path.exists(fdict['path']):
            os.remove(fdict['path'])
        db.session.delete(file)

    if os.path.exists(folder.path):
        os.rmdir(folder.path)
    db.session.delete(folder)
    
    db.session.commit()

    return jsonify(success=True)


@files_bp.route('/upload')
def upload():
    return jsonify('yes')


@files_bp.route('/folder/download/<folder_id>')
def download_folder(folder_id):
    return jsonify('return_folder')


@files_bp.route('/download/<file_id>')
def download(file_id):
    return jsonify('return_folder')

