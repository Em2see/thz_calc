from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_debugtoolbar import DebugToolbarExtension
import os
import logging
import sys

db = SQLAlchemy()
# login_manager = LoginManager()


def create_app():
    app = Flask(__name__, static_folder=None, instance_relative_config=True)
    app.logger.setLevel(logging.INFO)

    if 'FLASK_CONFIG' in os.environ:
        app.config.from_envvar('FLASK_CONFIG')
    else:
        # print(os.environ)
        environment_configuration = os.environ.get('CONFIGURATION_SETUP')
        print('>>>>>>>', environment_configuration)
        app.config.from_object(environment_configuration)

    # login_manager.init_app(app)
    # login_manager.login_view = 'login_bp.login'
    toolbar = DebugToolbarExtension(app)

    db.init_app(app)

    app_blueprints(app, login_manager=None)

    return app


def app_blueprints(app, login_manager=None):
    with app.app_context():
        from .files_bp import files_bp
        from .main_bp import main_bp
        # from . import thz_bp

        app.register_blueprint(main_bp)
        app.register_blueprint(files_bp, url_prefix='/files')

    # @login_manager.user_loader
    #     def load_user(id):
    #         return User.query.get(int(id))
