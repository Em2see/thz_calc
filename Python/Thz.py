#!/usr/bin/env python
# coding: utf-8
import numpy as np
import pandas as pd
import os
import math
import re
import sys
from scipy import stats
import seaborn as sns
import matplotlib.pyplot as plt
import itertools
import pathlib
from scipy.optimize import differential_evolution
from numba import jit
import yaml
import logging
import logging.config
import argparse
from glob import glob

from github import Github
import requests

from tqdm import tqdm

DEFAULT_PATH = os.path.abspath("./")
CONFIG_PATH = os.path.join(DEFAULT_PATH, "./config.yml")
SCRIPT_NAME_TEMPLATE = 'thz_{}.py'

logger = None


def configureLogger(config):
    """
    Configure logger based on input config 
    we check if there is console or file required.
    :return: 
        logger
    """
    # create logger with 'spam_application'
    
    handlers = config['logger'].get("handlers")
    if(handlers):
        for handler in handlers.values():
            if(issubclass(eval(handler["class"]),logging.FileHandler)):
                if not os.path.isabs(handler["filename"]):
                    handler["filename"] = os.path.join(DEFAULT_PATH, handler["filename"])
                filepath = pathlib.Path(handler["filename"])
                if ((not filepath.exists()) and (not filepath.is_dir())):
                    pathlib.Path(filepath.parent).mkdir(parents=True, exist_ok=True) 
                    pathlib.Path(filepath).touch()
        logging.config.dictConfig(config['logger'])
        del config['logger']
    else:
        FORMAT = '%(asctime)-15s %(clientip)s %(user)-8s %(message)s'
        logging.basicConfig(format=FORMAT)
    logger = logging.getLogger('simpleExample')
    return logger


def check_version(version, remote_hash):
    return version[1] != remote_hash

    
def update_confirm():
    is_ok = False
    update = False
    while not is_ok:
        update = input('You are using an outdated script. [Y/n]\n')
        is_ok = True
        if (update[0].lower != 'y') or (update is None):
            update = True
        elif update[0].lower != 'n':
            pass
        else:
            is_ok = False
    
    return update


def update_version(github_config, version):
    '''
        TODO: we need to fix this approach.
        I don't like it. I thinkg we need to replace the existing file.
    '''
    
    g = Github(github_config['key'])
    
    repo = g.get_user().get_repo(github_config['repo_name'])
    contents = repo.get_contents(github_config['file_path'])
    
    if check_version(version, contents.sha) and update_confirm():
        date = contents.last_modified
        date = re.sub('[ :]', '_', date[5:-4])
        url = contents.download_url
        new_code = requests.get(url)

        with open(os.path.join(DEFAULT_PATH, SCRIPT_NAME_TEMPLATE.format(date)), 'w+') as new:
            new.write(new_code.text)

        with open(CONFIG_PATH, 'w+', encoding='UTF-8') as version_info:
            version_info.write(contents.sha)
        
        logger.info('Update completed. Look for a new version in the current script folder')
        quit()

        
def open_config(config_path):
    with open(config_path, 'rb') as ymlfile:
        config = yaml.load(ymlfile, Loader=yaml.FullLoader)
    
    # paths
    for k, path in config.items():
        if (k[-5:] == '_path') and (not os.path.isabs(path)):
            config[k] = os.path.join(DEFAULT_PATH, path)
       
    # defaults 
    config['temperature'] = 25
    #config['eps'] = 2.5
    #config['s0'] = 0
    config['path_length'] = 0.005024
    
    return config


def parse_args(args):
    config = {
        'description': 'it\'s a script!',
        'args': [
           ('-e',
           {
           'dest': 'params',
           'action': 'store_true',
           'help': 'calculate the Ew spectra',
           'required': False
           }),
           ('-s',
           {
           'dest': 'statistics',
           'action': 'store_true',
           'help': 'compare spectra statisticaly',
           'required': False
           }),
           ('-p',
           {
           'dest': 'six_pack',
           'action': 'store_true',
           'help': 'calculate optimizations and six main parameters',
           'required': False
           }),
        ]
    }

    parser = argparse.ArgumentParser(description=config['description'])
    for name,details in config['args']:
        parser.add_argument(name,**details)
    
    return parser.parse_args(args)


def parameters(config):
    print('E-specters calculation begins')
    
    ea = pd.read_excel(os.path.join(DEFAULT_PATH, './Ea.xlsx'), engine = 'openpyxl')
    ea = ea[ea['Wavenumber (cm-1)'].between(*config['wavelength_range'])]
    ea_1 = ea['e1']
    ea_2 = ea['e2']

    files_paths = glob(os.path.join(config['refractive_path'], "*.csv")) #Got alphabetically sorted files in n-folder

    files = []
    for file_path in files_paths:
        file_name, _ = os.path.splitext(os.path.basename(file_path))
        sample = file_name.split(' ')[0]
        files.append((file_path, "".join([file_name, _]), sample))
    
    samples = list(set([file[2] for file in files]))

    if len(samples) <= 3:
        writer = pd.ExcelWriter(os.path.join(config['calcs_path'], 'Result_combined.xlsx'))
        
    for sample in samples:            
        result = {k:dict() for k in ['e1', 'e2', 'e1_K', 'e2_K']}
        i= 0

        for file_path, file_name, file_sample in files:
            if file_sample == sample:
                n = pd.read_csv(os.path.join(config['refractive_path'], file_name), skiprows=1)   #reading the files
                tr = pd.read_csv(os.path.join(config['transmittance_path'], file_name), skiprows=1)

                #cutting everything beyond 10 and above 100 cm-1
                n = n[n['Wavenumber (cm-1)'].between(*config['wavelength_range'])]
                n = n + config['offset']
                tr = tr[tr['Wavenumber (cm-1)'].between(*config['wavelength_range'])]

                #A complete Maxwell-Garnett calculation
                
                e1 = n['Refractive Index']**2-(np.log(tr['Transmittance'])/(4*math.pi*n['Wavenumber (cm-1)']*config['path_length']))**2
                e2 = -2*n['Refractive Index']*np.log(tr['Transmittance'])/(4*math.pi*n['Wavenumber (cm-1)']*config['path_length'])

                f = 0.001*config['conc']*config['volume'] #Volume share

                e1 = e1.to_numpy()
                e2 = e2.to_numpy()

                a = ea_1*(1+2*f)-e1*(2+f)
                b = ea_2*(1+2*f)-e2*(2+f)
                c = 4*(e1**2-e2**2)*(1+f)+(ea_1**2-ea_2**2)*(1+4*f)+2*(ea_1*e1-ea_2*e2)*(2-13*f)
                d = 2*(e1*(4*e2*(1+f)+ea_2*(2-13*f))+ea_1*(ea_2*(1+4*f)+e2*(2-13*f)))
                e = 4*(f-1)

                e1_K = 1/e*(a-np.sqrt((c+np.sqrt(c**2+d**2))/2))
                e2_K = 1/e*(b-np.sqrt((-c+np.sqrt(c**2+d**2))/2))

               
                #e2_K = (((3*0.00071*config['conc']-1)*2.89*e1+2*(e2**2-e1**2))*(2-3*0.00071*config['conc'])*e2-(2.89+(2-3*0.00071*config['conc'])*e1)*((3*0.00071*config['conc']-1)*2.89*e2-4*e1*e2))/(((2-3*0.00071*config['conc'])*e2)**2+(2.89+(2-3*0.00071*config['conc'])*e1)**2)
                #e1_K = e2_K*((2-3*0.00071*config['conc'])*e2)/(2.89+(2-3*0.00071*config['conc'])*e1)-((3*0.00071*config['conc']-1)*2.89*e1+2*(e2**2-e1**2))/(2.89+(2-3*0.00071*config['conc'])*e1)

                result['e1'][f'e1_{i + 1}'] = e1
                result['e2'][f'e2_{i + 1}'] = e2
                result['e1_K'][f'e1_K_{i + 1}'] = e1_K
                result['e2_K'][f'e2_K_{i + 1}'] = e2_K
                i += 1

        if len(samples) > 3:
            writer = pd.ExcelWriter(os.path.join(config['calcs_path'], 'Result_{:s}.xlsx'.format(sample)))

        for k, value in result.items():
            tmp_df = pd.concat([n['Wavenumber (cm-1)'].reset_index(drop=True), pd.DataFrame(value).reset_index(drop=True)], axis=1)
            tmp_df.to_excel(writer, sheet_name="{:s}_{:s}".format(sample, k), index=False)
        
        if len(samples) > 3:
            writer.close()
            
    if len(samples) <= 3:
        writer.close()
        
    print('\nDone!')


# ### Statistical comparison

def welch(a, b):
    return (stats.ttest_ind(a, b, equal_var = False)[1])


def statistic(config):
    samples = []

    files = glob(os.path.join(config['calcs_path'], '*.xlsx'))

    for file_path in files:
        samples += pd.ExcelFile(file_path, engine='openpyxl').sheet_names

    flat_list = list(set([re.sub('_e1_K|_e2_K|_e1|_e2', '', item) for item in samples]))

    print('We have found the following samples:\n')

    for n,i in enumerate(flat_list):
        print(n,i)

    print('\nWhich ones do you want to compare?')

    select = input('\nType the numbers, separated with comma (they will be compared pairwise). Type "exit" to exit the script: ')
    
    if select.lower() == 'exit':
        raise ValueError('Interrupted by user')
    
    select = re.sub(' ', '', select)
    select = select.split(',')

    select = [[i for n,i in enumerate(flat_list) if n == int(sel)] for sel in select]
    select = [item for sublist in select for item in sublist]

    pairs = list(itertools.combinations(select, 2))
    
    print('\n\nChoose pairs that you want to drop')
    
    for n,i in enumerate(pairs):
        print(n,i)
    
    print('\n')
    blacklist = input('Type the numbers, separated with comma. If none, type "n" and ENTER: ')
    blacklist = re.sub(' ', '', blacklist)
    
    if blacklist.lower() != 'n':
        
        blacklist = blacklist.split(',')
        pairs = [[i for n,i in enumerate(pairs) if n == int(drop)] for drop in blacklist]
        pairs = [item for sublist in pairs for item in sublist]
    
    progress = 4*len(pairs)
    iter_count = 0
    
    print('\nThe progress bar will appear as soon as the first comparison will be made. Whait a second or two...\n')
    
    for pair in pairs:
        sample_1_sheets = dict()
        sample_2_sheets = dict()
        
        for file_path in files:
            exl = pd.ExcelFile(file_path, engine='openpyxl')

            for i in exl.sheet_names:
                if pair[0] in i:
                    for param in ['e1', 'e2', 'e1_K', 'e2_K']:
                        if i.endswith(param):
                            sample_1_sheets[param] = pd.read_excel(file_path, sheet_name=i, engine='openpyxl')
                if pair[1] in i:
                    for param in ['e1', 'e2', 'e1_K', 'e2_K']:
                        if i.endswith(param):
                            sample_2_sheets[param] = pd.read_excel(file_path, sheet_name=i, engine='openpyxl')
        
        for param in tqdm(sample_1_sheets.keys()):   
             
            s1 = pd.melt(sample_1_sheets[param], id_vars = 'Wavenumber (cm-1)')
            s1['Sample'] = pair[0]
            s1.columns = ['Wavenumber', 'Parameter', 'value', 'Sample']

            s2 = pd.melt(sample_2_sheets[param], id_vars = 'Wavenumber (cm-1)')
            s2['Sample'] = pair[1]
            s2.columns = ['Wavenumber', 'Parameter', 'value', 'Sample']

            pvals = pd.DataFrame({'Wavenumber': s1['Wavenumber'].unique(), 
                                  'p-value': [welch(group['value'][group['Sample'] == pair[0]], group['value'][group['Sample'] == pair[1]]) for name, group in pd.concat([s1, s2], axis=0).groupby('Wavenumber')]})

            sns.lineplot(x="Wavenumber", y="value", hue='Sample', data=pd.concat([s1, s2], axis=0))
            ax2 = plt.twinx()
            sns.lineplot(x="Wavenumber", y="p-value", data=pvals, color="b", ax=ax2, legend=False)

            if pvals.max()['p-value'] > 0.05:
                ax2.axhline(0.05, ls='--')

            plt.savefig(config['stats_path'] + pair[0] + '_'+ pair[1] + '_'+param+ '.png', dpi=300)
            plt.clf()

            pvals.to_excel(config['stats_path'] + pair[0]+'_'+pair[1]+ '_'+param+'.xlsx', index=False)


@jit(nopython=True)
def to_optim(p0, x, e1_K_i, e2_K_i, eps, t1, s0):
    de1, de2, t2, A, w2, g = p0
    
    e1_mod = eps + de1/(1.0 + (0.1885*t1*x)*(0.1885*t1*x)) + de2/(1.0 + (0.1885*t2*x)*(0.1885*t2*x)) + (A*(w2*w2-x*x))/((w2*w2-x*x)*(w2*w2-x*x)+(g*x)*(g*x))
    e2_mod = (0.1885*de1*t1*x)/(1 + (0.1885*t1*x)*(0.1885*t1*x)) + (0.1885*de2*t2*x)/(1.0 + (0.1885*t2*x)*(0.1885*t2*x)) + (A*g*x)/((w2*w2-x**2)*(w2**2-x**2)+ (g*x)*(g*x)) + 0.6*s0/x
    mse = np.sqrt(np.sum(((e2_K_i-e2_mod)/e2_mod)**2 + ((e1_K_i-e1_mod)/e1_mod)**2)) / len(x)
    
    return (mse)


def six_pack(config):
    print('Optimization started. Waiting for ', config['confirmations'], ' confirmations of minimum\nNOTE: Confirmations may reset if new minimum will be found')
    
    files = [file_path for file_path in glob(os.path.join(config['calcs_path'], 'Result*.*'))]

    #Debay t1 calculation from temperature
    t1 = 412.7754/(273.16+config['temperature'])*((273.16+config['temperature'])/219.90747-1)**(-1.7313)
    
    for file_path in files:
        excel = pd.ExcelFile(file_path, engine='openpyxl')
        unique = list(set([re.sub('_e[\d+]_K|_e[\d+]', '',i) for i in excel.sheet_names]))

        for sample in unique:
            mins = pd.DataFrame()
            for sheet in excel.sheet_names:
                if sheet.startswith(sample):
                    if sheet.endswith('e1_K'):
                        e1_K = pd.read_excel(file_path, sheet_name=sheet, engine="openpyxl")
                    if sheet.endswith('e2_K'):
                        e2_K = pd.read_excel(file_path, sheet_name=sheet, engine="openpyxl")

            for repeat in range(1, len(e1_K.columns)):
                print(sample, repeat)

                e2_K_i = e2_K['e2_K_'+str(repeat)].to_numpy()
                e1_K_i = e1_K['e1_K_'+str(repeat)].to_numpy()

                x = e1_K['Wavenumber (cm-1)'].to_numpy()

              
                #Start optimization process
                p0 = [48, 2, 0.2, 35000, 150, 280]
                incr = 0
                result_fun = np.full(shape=(config['confirmations']*100+incr, 1), fill_value=999.9)
                result_params = np.zeros(shape=(config['confirmations']*100+incr, 6))
                confirmations = 0
                i = 0
                previous_min = 100

                try:
                    while confirmations < config['confirmations']: 

                        res = differential_evolution(to_optim, args = (x, e1_K_i, e2_K_i, config['eps'], t1, config['s0']),
                                                     tol=1e-8,
                                                     bounds=[tuple(config['constrains'].get('param_de1')),
                                                             tuple(config['constrains'].get('param_de2')),
                                                             tuple(config['constrains'].get('param_t2')),
                                                             tuple(config['constrains'].get('param_A')),
                                                             tuple(config['constrains'].get('param_w2')),
                                                             tuple(config['constrains'].get('param_g'))])
                        result_fun[i] = res.fun
                        result_params[i] = res.x
                        previous_min = result_fun.min()

                        if res.fun < previous_min:
                            previous_min = res.fun
                        elif res.fun == previous_min:
                            confirmations += 1
                            print('Confirmation ', confirmations, ' of ', config['confirmations'])
                        i += 1

                except Exception as e:
                    print(e)
                    incr += 100

                min_params = result_params[np.argmin(result_fun)]
                min_loss = result_fun[np.argmin(result_fun)]

                mins = pd.concat([mins, pd.DataFrame({'sample': sample, 
                                             'repeat': repeat,
                                             'de1': min_params[0],
                                             'de2': min_params[1],
                                             't2': min_params[2],
                                             'A': min_params[3],
                                             'w2': min_params[4],
                                             'g': min_params[5],
                                             'loss': min_loss})], ignore_index=True)

                plt.plot(x, config['eps'] + min_params[0]/(1 + (0.1885*t1*x)*(0.1885*t1*x)) + min_params[1]/(1 + (0.1885*min_params[2]*x)**2) + (min_params[3]*(min_params[4]**2-x**2))/((min_params[4]**2-x**2)*(min_params[4]**2-x**2)+(min_params[5]*x)**2))
                plt.plot(x, e1_K_i) 
                plt.savefig(config['optim_path'] + sample + '_' + str(repeat) + '_e1.png', dpi=300)
                plt.clf()

                plt.plot(x, (0.1885*min_params[0]*t1*x)/(1 + (0.1885*t1*x)*(0.1885*t1*x)) + (0.1885*min_params[1]*min_params[2]*x)/(1.0 + (0.1885*min_params[2]*x)**2) + (min_params[3]*min_params[5]*x)/((min_params[4]**2-x**2)*(min_params[4]**2-x**2)+ (min_params[5]*x)**2) + 0.6*config['s0']/x)
                plt.plot(x, e2_K_i) 
                plt.savefig(config['optim_path'] + sample + '_' + str(repeat) + '_e2.png', dpi=300)
                plt.clf()

            mins.to_excel(config['optim_path'] + sample+'.xlsx', engine='openpyxl', index=False)
    print('\nDone!')

     
def main(args):
    # do we need to upgrade?
    # we could use another config if needed.
    config = open_config(CONFIG_PATH)
    logger = configureLogger(config)
    for arg, val in vars(args).items():
        if arg == 'statistics' and val:
            statistic(config)
        if arg == 'params' and val:
            parameters(config)
        if arg == 'six_pack' and val:
            six_pack(config)

        
if __name__ == "__main__":
    args = parse_args(sys.argv[1:])
    main(args)
