## Resources

### Top 50 ggplot Visualizations with examples:
[link](http://r-statistics.co/Top50-Ggplot2-Visualizations-MasterList-R-Code.html)

### A tutorial for GAM and GLM:
[link](https://m-clark.github.io/generalized-additive-models/introduction.html)

### Оценка модели:
[link](https://r-analytics.blogspot.com/2016/12/blog-post.html)

### Книга на русском "Анализ временных рядов в R" (2020), Мастицкий С.Э.
[link](https://ranalytics.github.io/tsa-with-r/)

### PCA step-by-step:
[link](https://builtin.com/data-science/step-step-explanation-principal-component-analysis)
