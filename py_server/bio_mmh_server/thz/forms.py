from django import forms
from loguru import logger


class MultiWidgetBasic(forms.widgets.MultiWidget):
    def __init__(self, attrs=None):

        widgets = [forms.TextInput(),
                   forms.TextInput()]
        super(MultiWidgetBasic, self).__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            return value.split(';')
        else:
            return ['0', '0']


class WaveLengthField(forms.MultiValueField):
    widget = MultiWidgetBasic

    def __init__(self, **kwargs):
        logger.debug(kwargs)
        error_messages = {
            'incomplete': 'Введите обе длины волны',
        }

        fields = (
            forms.FloatField(
                label='Начальное значение',
                error_messages={'incomplete': 'Ведите начальную длину волны'},
                validators=[],
                min_value=0,
                max_value=330,
            ),
            forms.FloatField(
                label='Конечное значение',
                error_messages={'incomplete': 'Ведите конечную длину волны'},
                validators=[],
                min_value=0,
                max_value=330,
            ),
        )
        super().__init__(
            error_messages=error_messages, fields=fields,
            require_all_fields=True, **kwargs
        )

    def compress(self, values):
        return ";".join(map(str, values))


class ThzForm(forms.Form):
    refractive_files = forms.FileField(
        label='Коэффициенты преломления',
        widget=forms.ClearableFileInput(attrs={'multiple': True}),
        required=True)
    transmittance_files = forms.FileField(
        label='Пропускание',
        widget=forms.ClearableFileInput(attrs={'multiple': True}),
        required=True)
    path_length = forms.FloatField(label='Оптический путь', required=True)
    concentration = forms.FloatField(label='Концентрация', required=True)
    wave_length = WaveLengthField(label='Диапазон длин волн', required=True)

