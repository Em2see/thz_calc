from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import FormView
from django.urls import reverse, reverse_lazy
from loguru import logger
from bio_mmh_server.views import BaseMixin
from files.views import CalcsMixin
from .forms import ThzForm


class ThzView(LoginRequiredMixin, BaseMixin, CalcsMixin, FormView):
    template_name = 'thz/thz.html'
    form_class = ThzForm
    success_url = reverse_lazy('thz')

    def get_initial(self):
        """
        Returns the initial data to use for forms on this view.
        """
        initial = super().get_initial()

        initial['path_length'] = 0.005024
        initial['concentration'] = 0
        initial['wave_length'] = "0;200"

        return initial

    def form_valid(self, form):

        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('index')
