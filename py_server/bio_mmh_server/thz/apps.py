from django.apps import AppConfig


class ThzConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'thz'
