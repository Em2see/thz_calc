import pandas as pd
from typing import List


def aggregate_csv_files(files_list: List[str]) -> pd.DataFrame:
    out_df = pd.DataFrame()

    for path in files_list:
        df = pd.read_csv(path)
        out_df = pd.concat([out_df, df], axis=1, ignore_index=True)

    return out_df