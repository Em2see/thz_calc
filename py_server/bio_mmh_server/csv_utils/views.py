import io
import zipfile

from django.http import HttpResponse, HttpResponseNotFound
from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import FormView
from django.urls import reverse, reverse_lazy
from loguru import logger
from django.views import View
from bio_mmh_server.views import BaseMixin
from bio_mmh_server import celery_app
from files.views import CalcsMixin
from files.tasks import CalculationTask
from files.models import Computation, SourceFile, ResultFile
from .forms import CSVForm
from .utils import aggregate_csv_files


@celery_app.task(bind=True, base=CalculationTask)
def csv_calcs(self, calc_id):
    calc = Computation.objects.get(pk=calc_id)
    try:
        calc.status = Computation.PROCESSING
        calc.save()

        # collect files
        file_paths = (f.read_file() for f in calc.sourcefile_related.all())

        df = aggregate_csv_files(file_paths)

        out_file = io.BytesIO()
        df.to_csv(out_file, index=False)
        out_file = ResultFile.write_file(out_file, "aggregated_file.csv", calc)
        file_id = out_file.id
        calc.status = Computation.SUCCESS
        calc.save()

    except Exception as ex:
        logger.error("Computation id=" + str(calc_id))
        logger.error(ex)

        calc.status = Computation.FAILED
        calc.save()
        file_id = -1

    return file_id


def get_result_file(request, pk):
    calc = Computation.objects.get(pk=pk)
    if calc.resultfile_related.count() > 1:
        return get_result_files(request, pk)

    try:
        result_file = calc.resultfile_related.first()
        file = result_file.read_file()
        file_data = file.read()

        # sending response
        response = HttpResponse(file_data, content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="{:s}"'.format(result_file.file_name)

    except IOError:
        # handle file not exist case here
        response = HttpResponseNotFound('<h1>File not exist</h1>')

    return response


def get_result_files(request, pk):
    return get_files(request, pk)


def get_source_files(request, pk):
    return get_files(request, pk, sources=True)


def get_files(request, pk, sources=False):
    calc = Computation.objects.get(pk=pk)
    if sources:
        out_filename = "sources.zip"
        files_list = calc.sourcefile_related
    else:
        out_filename = "results.zip"
        files_list = calc.resultfile_related

    try:
        zip_buffer = io.BytesIO()
        with zipfile.ZipFile(zip_buffer, "a", zipfile.ZIP_DEFLATED, False) as zip_file:
            for file in files_list.all():
                zip_file.writestr(file.file_name, file.read_file().read())
        zip_data = zip_buffer.getvalue()

        # sending response
        response = HttpResponse(zip_data, content_type='application/zip')
        response['Content-Disposition'] = 'attachment; filename="{:s}"'.format(out_filename)
    except IOError:
        # handle file not exist case here
        response = HttpResponseNotFound('<h1>File not exist</h1>')

    return response


class CSVView(LoginRequiredMixin, BaseMixin, CalcsMixin, FormView):
    template_name = 'csv/index.html'
    form_class = CSVForm
    success_url = reverse_lazy('csv')

    def get_initial(self):
        """
        Returns the initial data to use for forms on this view.
        """
        initial = super().get_initial()

        return initial

    def form_valid(self, form):
        calc = Computation(module_name=self.module_name(), user=self.request.user)
        calc.save()
        files = self.request.FILES.getlist('files')
        for file in files:
            SourceFile.write_file(file.file, file_name=file.name, computation=calc)

        csv_calcs.delay(calc_id=calc.id)

        return super().form_valid(form)
