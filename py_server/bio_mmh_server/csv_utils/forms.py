from django import forms
from loguru import logger


class CSVForm(forms.Form):
    files = forms.FileField(
        label='CSV files',
        widget=forms.ClearableFileInput(attrs={'multiple': True}),
        required=True)
