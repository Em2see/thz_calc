from django.db import models
from django.utils.translation import gettext as _
from django.core.validators import RegexValidator, MinValueValidator, MaxValueValidator
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.db.models.signals import post_save
from django.dispatch import receiver

from loguru import logger


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email = models.CharField(max_length=100, default='', verbose_name='email')

    def __str__(self):
        return f"{self.user}"


@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
    logger.debug('Сработал create_profile')
    if created:
        logger.debug(f'Создан Profile для username: {instance.username}')
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    logger.debug('Сработал save_user_profile')
    instance.profile.save()

