from django.contrib import admin

from .models import Profile


class ProfileAdmin(admin.ModelAdmin):
    model = Profile
    list_per_page = 20


admin.site.register(Profile, ProfileAdmin)
