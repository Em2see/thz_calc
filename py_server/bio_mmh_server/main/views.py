from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse, reverse_lazy, NoReverseMatch
from django.views.generic import TemplateView, CreateView, UpdateView, ListView
from .models import Profile
from loguru import logger
from bio_mmh_server.views import BaseMixin


class IndexView(TemplateView, BaseMixin):
    template_name = 'main/index.html'


class ProfileUpdateView(LoginRequiredMixin, BaseMixin, UpdateView):
    model = Profile
    # form_class = ProfileForm
    fields = ['email']
    login_url = reverse_lazy('login')
    redirect_field_name = 'redirect_to'
    template_name = 'auth/update_user.html'

    # TODO modify first_name, last_name in User model
    def get_object(self):
        logger.info(self.request.user)
        obj = self.model.objects.get(user=self.request.user)
        logger.info(obj)
        return obj

    def get_success_url(self):
        return reverse_lazy('index')


class RegisterUserView(CreateView):
    """
    Представление для регистрации
    """
    form_class = UserCreationForm
    template_name = 'auth/register.html'
    success_url = reverse_lazy('index')

    def form_valid(self, form):
        valid = super(RegisterUserView, self).form_valid(form)
        user = authenticate(
            request=self.request,
            username=form.cleaned_data['username'],
            password=form.cleaned_data['password1']
        )
        login(self.request, user)

        return valid
