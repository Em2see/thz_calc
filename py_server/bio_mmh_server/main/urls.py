from django.contrib.auth.views import (LogoutView, LoginView)
from django.urls import path
from .views import IndexView, RegisterUserView, ProfileUpdateView
from thz.views import ThzView
from csv_utils.views import CSVView


urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('register/', RegisterUserView.as_view(), name='register'),
    path('login/', LoginView.as_view(template_name='auth/login.html', ), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('profile/', ProfileUpdateView.as_view(), name='profile'),
    path('csv/', CSVView.as_view(), name='csv'),
    path('thz/', ThzView.as_view(), name='thz'),
]
