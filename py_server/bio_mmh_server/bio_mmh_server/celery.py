from __future__ import absolute_import
import os
from celery import Celery, Task

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'bio_mmh_server.settings')

# здесь вы меняете имя
app = Celery("bio_mmh_server")

# Для получения настроек Django, связываем префикс "CELERY" с настройкой celery
app.config_from_object('django.conf:settings', namespace='CELERY')

# загрузка tasks.py в приложение django
app.autodiscover_tasks()
