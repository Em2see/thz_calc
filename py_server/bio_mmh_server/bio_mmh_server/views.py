from django.views.generic.base import ContextMixin
from django.urls import reverse, NoReverseMatch
from loguru import logger


class MenuContextMixin(ContextMixin):

    def get_context_data(self, **kwargs):
        context = super(MenuContextMixin, self).get_context_data(**kwargs)
        context['menu'] = [('index', 'Home')]
        context['auth_menu'] = []
        auth_menu = [('thz', 'Terahertz'),
                     ('csv', 'CSV'),
                     ('files', 'Files'),]
        for url_name, menu_name in auth_menu:
            try:
                reverse(url_name)
                context['auth_menu'].append((url_name, menu_name))
            except NoReverseMatch as ex:
                logger.error(ex)

        return context


class BaseMixin(MenuContextMixin):
    """
    Mixin for all pages
    """