from django.db import models
from django.utils.translation import gettext as _
from django.utils.timezone import datetime
from django.utils import timezone
from django.contrib.auth.models import User

from loguru import logger
from datetime import timedelta
import uuid

DEFAULT_FOLDER_PATH_LEN = 256
DEFAULT_FILE_PATH_LEN = 64
DEFAULT_NAME_LEN = 64
FOLDER_LIFETIME = timedelta(weeks=1)


def get_default_expire():
    return datetime.now() + FOLDER_LIFETIME


class Computation(models.Model):
    CREATED = 0
    PROCESSING = 1
    FAILED = 2
    SUCCESS = 3

    STATUS = (
        (CREATED, "Created"),
        (PROCESSING, "Processing"),
        (FAILED, "Failed"),
        (SUCCESS, "Success"),
    )

    name = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    module_name = models.CharField(max_length=256)
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE,
                             verbose_name=_("User"), related_name="computations")
    params = models.CharField(max_length=256)
    status = models.PositiveIntegerField(choices=STATUS, default=CREATED)
    date_added = models.DateTimeField(auto_now_add=True)
    date_created = models.DateTimeField(default=timezone.now)
    date_expire = models.DateTimeField(default=get_default_expire)

    def get_result_str(self):
        return ",".join([f.file_name for f in self.resultfile_related.all()])

    def get_source_str(self):
        return "; ".join([f.file_name for f in self.sourcefile_related.all()])


class FileReportAbs(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    file = models.FileField(upload_to='media')
    computation = models.ForeignKey(Computation,
                                    on_delete=models.CASCADE,
                                    related_name="%(class)s_related",
                                    related_query_name="%(class)ss",)
    file_name = models.CharField(max_length=128, null=True, blank=True)

    def __str__(self):
        return f"{self.file_name}"

    @classmethod
    def write_file(cls, file_descriptor, file_name, computation):
        file_path = cls.file.field.generate_filename(None, file_name)
        res = cls.file.field.storage.save(file_path, file_descriptor)
        file = cls(computation=computation,
                   file=res,
                   file_name=file_name)
        file.save()
        return file

    def read_file(self):
        fl = self.file.field.storage.open(self.file.name)
        return fl

    class Meta:
        abstract = True


class SourceFile(FileReportAbs):
    pass


class ResultFile(FileReportAbs):
    pass
