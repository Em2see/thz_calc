from bio_mmh_server import celery_app
from .models import Computation
import json


class CalculationTask(celery_app.Task):
    def on_success(self, retval, task_id, args, kwargs):
        calc_id = kwargs['calc_id']
        Computation.objects.filter(id=calc_id)
