from django.contrib import admin
from .models import SourceFile, ResultFile, Computation


class SourceFileAdmin(admin.ModelAdmin):
    model = SourceFile
    list_per_page = 20


class ResultFileAdmin(admin.ModelAdmin):
    model = ResultFile


class ComputationAdmin(admin.ModelAdmin):
    model = Computation


admin.site.register(SourceFile, SourceFileAdmin)
admin.site.register(ResultFile, ResultFileAdmin)
admin.site.register(Computation, ComputationAdmin)
