from django.contrib.auth.views import (LogoutView, LoginView)
from django.urls import path
from .views import ViewFile, ViewProject, ViewProjects, FilesAddFormView
from csv_utils.views import get_result_file, get_source_files


urlpatterns = [
    path('', ViewProjects.as_view(), name='files'),
    path('project/<int:pk>', ViewProject.as_view(), name='files_project'),
    path('project/<int:pk>/result', get_result_file, name='calc_result'),
    path('project/<int:pk>/sources', get_source_files, name='calc_source'),
    path('remove/<int:pk>', ViewProject.as_view(), name='project_remove'),
    path('file/<int:pk>', ViewFile.as_view(), name='files_file'),
    path('add', FilesAddFormView.as_view(), name='files_add')
]
