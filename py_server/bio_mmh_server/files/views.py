from django.shortcuts import render
from django.views.generic.base import ContextMixin
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from django.views.generic.edit import FormView
from django.urls import reverse
from loguru import logger


from .forms import AddFiles
from .models import Computation, ResultFile, SourceFile


MAX_TABLE_CALCS = 5


class ViewProjects(ListView):
    model = Computation
    template_name = 'files/index.html'
    context_object_name = 'projects'


class ViewProject(DetailView):
    model = Computation
    template_name = 'files/project_details.html'


class ViewFile(DetailView):
    model = ResultFile
    template_name = 'files/result_file_details.html'


class ViewSourceFile(DetailView):
    model = SourceFile
    template_name = 'files/source_file_details.html'


class FilesAddFormView(FormView):
    template_name = 'files/add_files.html'
    form_class = AddFiles
    success_url = '/files/'

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        logger.debug(form.files)
        return super().form_valid(form)


class CalcsMixin(ContextMixin):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        logger.info(args)
        logger.info(kwargs)

    def module_name(self):
        try:
            module_name = self.__class__.__module__.split('.')[0]
        except:
            module_name = "None"
        return module_name

    def get_context_data(self, **kwargs):
        context = super(CalcsMixin, self).get_context_data(**kwargs)
        context['results'] = Computation.objects\
                .filter(user=self.request.user, module_name=self.module_name())\
                .order_by('-date_created')\
                .all()[:MAX_TABLE_CALCS]

        return context
