from setuptools import setup, find_packages
# from setuptools.command.bdist_egg import bdist_egg as _bdist_egg
# from wheel.bdist_wheel import bdist_wheel

import sys
import os

def_path = '.'
# def_path = os.path.dirname(os.path.abspath(__file__))
package_path = def_path

if package_path not in sys.path:
    sys.path.append(package_path)


from bio_mmh import __version__, __author__, __email__


with open(os.path.join(def_path, "README.md"), "rb") as fh:
    long_description = fh.read().decode("utf-8")

with open(os.path.join(def_path, "requirements.txt"), "r") as fh:
    requirements = fh.readlines()

with open(os.path.join(def_path, "HISTORY.rst"), "rb") as history_file:
    history = history_file.read().decode("utf-8")

# data_files

setup(
    name="bio_mmh",
    version=__version__,
    author=__author__,
    author_email=__email__,
    license='MIT',
    description="Tool for spectrum calcs for GHz",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/PaulBobyrev/Biostatistics-MMH",
    packages=find_packages(where=def_path),
    package_dir={"": def_path},
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_data={
        'bio_mmh': [
            r'server\static_files\*.*', 
            r'server\static_files\scripts\jquery\*.*',
            r'server\static_files\scripts\semantic\dist\*.*',
            r'server\static_files\scripts\semantic\dist\*\*.*',
            r'server\static_files\scripts\semantic\dist\themes\default\assets\*\*.*'
            ]
    },
    include_package_data=True,
    python_requires='>=3.6',
    cmdclass={},
    entry_points={
        'console_scripts': [
            'bio_mmh = bio_mmh:cli',
        ]
    },
    install_requires=requirements,
    setup_requires=['wheel']
)