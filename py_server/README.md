## Инструмент для расчета

### 1. Сборка
    python setup.py bdist_wheel

### 2. Установка
    Это только первый подход, поэтому все очень 
    python -m venv test_env
    .\test_venv\Scripts\Activate.ps1
    pip install .\dist\bio_mmh-0.5.0-py3-none-any.whl
    #### На случай, если все установлено и нужно обновиться. 
    pip install --upgrade --no-deps .\dist\bio_mmh-0.5.0-py3-none-any.whl

### 3. Заупуск
    python bio_mmh

### 4. Create superuser
sudo docker exec -it mservices_web_1 /bin/sh